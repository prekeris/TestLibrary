﻿using NUnit.Framework;

namespace TestApplication.Tests.String.Tests
{
    [TestFixture]
    public class IStringTests
    {
        private TestLibrary.IStringService _service;

        [SetUp]
        public void TestInitialize()
        {
            _service = new TestLibrary.String();
        }

        [TestCase]
        public void IsFirstLetterCapitalizedShouldReturnFalse()
        {
            //arrange
            string param = "test";

            //act
            var response = _service.IsFirstLetterCapitalized(param);

            //assert
            Assert.IsFalse(response);
        }

        [TestCase]
        public void IsFirstLetterCapitalizedShouldReturnTrue()
        {
            //arrange
            var param = "Test";

            //act
            var response = _service.IsFirstLetterCapitalized(param);

            //assert
            Assert.IsTrue(response);
        }

        [TestCase]
        public void IsAllWordsFirstLetterCapitalizedShouldReturnFalse()
        {
            //arrange
            var param = "This Is Test sentence";

            //act
            var response = _service.IsAllWordsFirstLetterCapitalized(param);

            //assert
            Assert.IsFalse(response);
        }

        [TestCase]
        public void IsAllWordsFirstLetterCapitalizedShouldReturnTrue()
        {
            //arrange
            var param = "This Is Test Sentence";

            //act
            var response = _service.IsAllWordsFirstLetterCapitalized(param);

            //assert
            Assert.IsTrue(response);
        }
    }
}
