﻿using NUnit.Framework;

namespace TestApplication.Tests.String.Tests
{
    [TestFixture]
    public class StringTests
    {
        [TestCase]
        public void IsFirstLetterCapitalizedShouldReturnFalse()
        {
            //arrange
            string param = "test";

            //act
            var response = new TestLibrary.String().IsFirstLetterCapitalized(param);

            //assert
            Assert.IsFalse(response);
        }

        [TestCase]
        public void IsFirstLetterCapitalizedShouldReturnTrue()
        {
            //arrange
            var param = "Test";

            //act
            var response = new TestLibrary.String().IsFirstLetterCapitalized(param);

            //assert
            Assert.IsTrue(response);
        }

        [TestCase]
        public void IsAllWordsFirstLetterCapitalizedShouldReturnFalse()
        {
            //arrange
            var param = "This Is Test sentence";

            //act
            var response =new TestLibrary.String().IsAllWordsFirstLetterCapitalized(param);

            //assert
            Assert.IsFalse(response);
        }

        [TestCase]
        public void IsAllWordsFirstLetterCapitalizedShouldReturnTrue()
        {
            //arrange
            var param = "This Is Test Sentence";

            //act
            var response = new TestLibrary.String().IsAllWordsFirstLetterCapitalized(param);

            //assert
            Assert.IsTrue(response);
        }
    }
}
