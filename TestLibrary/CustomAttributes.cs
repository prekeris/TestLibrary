﻿using System;

namespace TestLibrary
{
    /// <summary>
    /// Attribute determines if parameter is null 
    /// </summary>
    public class NotNullOrEmptyAttribute : ArgumentValidationAttribute
    {
        public override void Validate(object value, string argumentName)
        {
            if (string.IsNullOrEmpty(value.ToString()))
            {
                throw new ArgumentNullException(argumentName);
            }
        }
    }
}
