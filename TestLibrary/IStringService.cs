﻿namespace TestLibrary
{
    public interface IStringService
    {
        bool IsFirstLetterCapitalized(string word);
        bool IsAllWordsFirstLetterCapitalized(string sentence);
    }
}
