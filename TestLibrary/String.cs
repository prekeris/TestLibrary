﻿using System;
using System.Linq;

namespace TestLibrary
{
    public class String : IStringService
    {
        /// <summary>
        /// Checks if the first letter of word is upper case
        /// <exception>
        /// Parameter word can not be null or empty
        /// </exception>
        /// </summary>
        public bool IsFirstLetterCapitalized([NotNullOrEmpty]string word)
        {
            return char.IsUpper(word[0]);
        }

        /// <summary>
        /// Checks if all the words in the sentence first letters are upper case
        /// </summary>
        /// <param name="sentence">
        /// Parameter witch represent the sentence
        /// <exception>
        /// Can not be null or empty
        /// </exception>
        /// </param>
        /// <returns>Returns if all sentence words first letters are upper case</returns>
        public bool IsAllWordsFirstLetterCapitalized([NotNullOrEmpty] string sentence)
        {
            var punctuation = sentence.Where(Char.IsPunctuation).Distinct().ToArray();
            var words = sentence.Split().Select(x => x.Trim(punctuation));
            return words.Select(IsFirstLetterCapitalized).OrderBy(w => w).First();
        }

    }


}
